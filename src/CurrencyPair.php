<?php

namespace tanuki\currency;

class CurrencyPair
{
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;

    /**
     * @param string $from
     * @param string $to
     */
    public function __construct(string $from, string $to)
    {
        $from = trim(mb_strtolower($from, 'utf-8'));
        $to = trim(mb_strtolower($to, 'utf-8'));

        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return "{$this->from}/{$this->to}";
    }

    /**
     * @return bool
     */
    public function validate()
    {
        //todo: add additional checks

        if ($this->from !== '' && $this->to !== '') {
            return true;
        }

        return false;
    }

}
