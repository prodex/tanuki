<?php

namespace tanuki\currency;

use tanuki\currency\storage\CacheStorageInterface;
use tanuki\currency\storage\DBStorageInterface;
use tanuki\currency\storage\WebStorageInterface;

class ExchangeRate
{
    /**
     * @var CacheStorageInterface
     */
    private $cacheStorage;
    /**
     * @var DBStorageInterface
     */
    private $dbStorage;
    /**
     * @var WebStorageInterface
     */
    private $webStorage;

    /**
     * @param CacheStorageInterface $cacheStorage
     * @param DBStorageInterface $dbStorage
     * @param WebStorageInterface $webStorage
     */
    public function __construct(CacheStorageInterface $cacheStorage, DBStorageInterface $dbStorage, WebStorageInterface $webStorage)
    {
        $this->cacheStorage = $cacheStorage;
        $this->dbStorage = $dbStorage;
        $this->webStorage = $webStorage;
    }

    /**
     * Get rate for currency pair.
     *
     * @param CurrencyPair $currencyPair
     *
     * @return bool|float
     */
    public function getRate(CurrencyPair $currencyPair)
    {
        if (!$currencyPair->validate()) {
             return false;
        }

        $rate = $this->cacheStorage->get($currencyPair);
        if ($rate === false) {
            $rate = $this->dbStorage->get($currencyPair);

            if ($rate === false) {
                $rate = $this->webStorage->get($currencyPair);
                if ($rate !== false) {
                    $this->dbStorage->set($currencyPair, $rate);
                }
            }

            if ($rate !== false) {
                $this->cacheStorage->set($currencyPair, $rate);
            }
        }

        return $rate !== false ? $rate : false;
    }

}
