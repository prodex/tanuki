<?php

namespace tanuki\currency\storage;

use tanuki\currency\CurrencyPair;

interface DBStorageInterface
{
    /**
     * @param CurrencyPair $currencyPair
     *
     * @return float|bool
     */
    public function get(CurrencyPair $currencyPair);

    /**
     * @param CurrencyPair $currencyPair
     * @param float $value
     *
     * @return bool
     */
    public function set(CurrencyPair $currencyPair, float $value);

}