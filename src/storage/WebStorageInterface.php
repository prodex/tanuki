<?php

namespace tanuki\currency\storage;

use tanuki\currency\CurrencyPair;

interface WebStorageInterface
{
    /**
     * @param CurrencyPair $currencyPair
     *
     * @return float|bool
     */
    public function get(CurrencyPair $currencyPair);

}
