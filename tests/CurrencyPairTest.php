<?php

namespace tanuki\currency\test;

use PHPUnit\Framework\TestCase;
use tanuki\currency\CurrencyPair;

class CurrencyPairTest extends TestCase
{
    public function testGetKey()
    {
        $currencyRate = new CurrencyPair('UsD', 'rub');
        $this->assertEquals('usd/rub', $currencyRate->getKey());

        $currencyRate = new CurrencyPair('eur', 'rub');
        $this->assertNotEquals('usd/rub', $currencyRate->getKey());

    }

    public function testValidate()
    {
        $currencyRate = new CurrencyPair('eur', 'usd');
        $this->assertTrue($currencyRate->validate());

        $currencyRate = new CurrencyPair('', 'rub');
        $this->assertFalse($currencyRate->validate());
    }

}
